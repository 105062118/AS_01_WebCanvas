# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

簡述:全部功能都有，還有一個額外的功能:筆觸

init(){
    在body的一開始就執行這個function，且將畫布給宣告好
    把一開始的cursor指定好，且進行第一次的cPush()，方便Undo到最前面的畫面
}

md(){
    當滑鼠點下去的時候所執行的function，將draw這個變數設定為true，方便判斷現在是否是需要畫圖或只是單純的滑鼠在上面移動
    包含了各種其他function的處理，將在後面的funtion做統一說明
}

mv(){
    當滑鼠移動的時候所執行的function，包含了各種其他function的處理
    將在後面的funtion做統一說明
}

mup(){
    當滑鼠按鍵放開的時候所執行的function，將draw這個變數設定為false，結束這一次畫圖
    包含了各種其他function的處理，將在後面的funtion做統一說明
}

changeclolor(){
    將畫筆的顏色設定為"colorblock"這個input設定的顏色
}

brush(){
    將cursor設定為畫筆模式，藉著將pen這個bool變數設定為true來判定，其他畫圓，三角形，方形等等都有類似的變數
    同時指定畫筆的粗細和顏色
    在畫筆模式時，md()所執行的功能是先選擇筆觸(之後會介紹，此處先介紹直線)
    直線時先moveTo()，將起點設置為滑鼠的位置，再來beginPath()，開始畫圖
    mv()所執行的是lineTo()，可以劃出一個線段來跟滑鼠的移動軌跡相符合，然後stroke()來正式畫出圖形
    mup()的工作是closePath()，可以將畫圖事件結束
}

addsize(){
    透過button將size這個變數++，然後賦予給畫筆的粗細跟畫其他圖形的粗細，最大到20
}

subsize(){
    透過button將size這個變數--，然後賦予給畫筆的粗細跟畫其他圖形的粗細，最小到2
}

eraser(){
    將模式調整成畫筆，筆觸改成線條，將顏色調整成背景色，達到清除的效果
}

draw_white(){
    使用clearRect()這個function來整個清除畫面，然後將背景色:白色塗上，然後再做push的動作，因為這沒有在canvas裡面觸發滑鼠事件
}


undo redo功能
首先創建一個陣列，用來儲存每一步的整個圖案
再來創立一個cStep，一開始初始化成-1，用來記錄現在跑到哪一步，是否還能繼續undo，redo

cPush(){
    將cStep++，且計算一下cStep和陣列目前所儲存的長度去做一個計算，如果返回後再繼續畫，則陣列的長度會跟
    cStep一樣，這樣能更貼近小畫家，再來就是將目前的整個canvas給儲存到陣列裡面
}

cUndo(){
    判斷如果cStep>0，那就可以執行undo，然後讀取陣列裡面的圖(依靠cStep知道要取哪一個)，然後將圖畫上去
}

cRedo(){
    判斷cStep是否有超過陣列的長度，如果是否則執行redo，然後讀取陣列裡面的圖(依靠cStep知道要取哪一個)，然後將圖畫上去
}


text_true(){
    將word改變為true，另外依據使用者所選的字型跟大小來決定其屬性
}

text(){
    在md()中，用fillText()這function來將文字貼上畫布，位置取決於滑鼠當前的位置
}

radio1()，radio2(),radio3(){
    用來改變字型
}

circle_true()，triangle_true()，rectangle_true(){
    決定現在要化的是哪一種圖形
}

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!多的特別功能
可以用來處理筆觸，有一般的線條，圓形方形還有三角形
draw_circle(){
    用來解決筆觸時的畫圖，利用arc()和fill()即可處理
}
draw_triangle(){
    用來解決筆觸時的畫三角形，利用moveTo()，搭配三個lineTo()和fill()即可處理
}

draw_rectangle(){
    用來解決筆觸時的畫圖，利用rect()和fill()即可處理
}

brush_cir_true()，brush_tri_true()，brush_rec_true()，brush_line_true(){
    都是用來決定現在的筆觸為何
}
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

image_true(){
    決定現在是否要將上傳的圖片貼上畫布
}

在每個function裡面都有改變他的cursor，不過相同功能不同形狀就用一樣的