var getc,c;
var temp = new Image();
var draw,pen = true,word = false,cir = false,tri = false,ima = false;
function init(){
    getc= document.getElementById('m');
    c = getc.getContext("2d");
    document.getElementById("m").style.cursor = "crosshair";
    c.rect(0,0,600,400);
    c.fillStyle = "white";
    c.fill();
    cPush();
}
function md(event){
    if(pen == true){
        draw = true;
        if(brush_line == true){
            c.moveTo(event.offsetX, event.offsetY);
            c.beginPath();
        }
        else if(brush_cir == true){
            draw_circle();
        }
        else if(brush_tri == true){
            draw_triangle(event);
        }
        else{
            draw_rectangle();
        }
    }
    else if(cir == true)
    {
        draw = true;
        startX = event.offsetX;
        startY = event.offsetY;
        c.strokeStyle = document.getElementById("colorblock").value;
    }
    else if((tri == true) || (rect == true)){
        draw = true;
        startX = event.offsetX;
        startY = event.offsetY;
    }
    else if(ima == true){
        draw = true;
        temp = document.getElementById("temp_img");
        now_picture.src = cPushArray[cStep];
        c.drawImage(temp,event.offsetX,event.offsetY,150,150);
    }
    else
        text(event);
}
function mv(event){
    if(pen == true){
        if(brush_line == true){
            if(draw){
            c.lineTo(event.offsetX,event.offsetY);
            c.stroke();
            }
        }
        else if(brush_cir == true){
            if(draw == true){
                draw_circle(event);
            }
        }
        else if(brush_tri == true){
            if(draw == true){
                draw_triangle(event);
            }
        }
        else{                       //brush_rec
            if(draw == true){
                draw_rectangle(event);
            }
        }
    }
    else if(cir == true){
        if(draw == true){
            var length =  Math.sqrt((Math.pow((startX-event.offsetX), 2)) + Math.pow((startY-event.offsetY), 2) );
            now_picture.src = cPushArray[cStep];
            c.clearRect(0,0,600,400);
            c.drawImage(now_picture, 0, 0);
            c.strokeStyle = document.getElementById("colorblock").value;
            c.beginPath();
            c.arc(startX,startY,length,0,2*Math.PI,false);
            c.stroke();
        }
    }
    else if(tri == true){
        if(draw == true){
            c.beginPath();
            now_picture.src = cPushArray[cStep];
            c.clearRect(0,0,600,400);
            c.drawImage(now_picture, 0, 0);
            c.strokeStyle = document.getElementById("colorblock").value;
            c.moveTo(startX,startY);
            c.lineTo(event.offsetX,event.offsetY);

            if(event.offsetX >= startX && event.offsetY >= startY)
                c.lineTo((2*startX - event.offsetX),event.offsetY);

            else if(event.offsetX < startX && event.offsetY >= startY)
                c.lineTo(event.offsetX,(2*startY - event.offsetY));

            else if(event.offsetX >= startX && event.offsetY < startY)
                c.lineTo(event.offsetX,(2*startY - event.offsetY));
                            
            else
                c.lineTo((2*startX - event.offsetX),event.offsetY);
            c.lineTo(startX,startY);
            c.lineTo(event.offsetX,event.offsetY);
            c.stroke();
            c.closePath();
        }
    }
    else if(rect == true){
        if(draw == true){
            c.beginPath();
            now_picture.src = cPushArray[cStep];
            c.clearRect(0,0,600,400);
            c.drawImage(now_picture, 0, 0);
            c.strokeStyle = document.getElementById("colorblock").value;
            c.rect(startX,startY,(event.offsetX - startX),(event.offsetY - startY));
            c.stroke();
            c.closePath();   
        }
    }
    else if(ima == true){
        if(draw == true){
            c.clearRect(0,0,600,400);
            c.drawImage(now_picture, 0, 0);
            c.drawImage(temp,event.offsetX,event.offsetY,150,150);
        }
    }
}
function mup(event){
    draw = false;
    if(pen == true)
    c.closePath();
    cPush();
}
//colorblock--------------
function changecolor(){
    c.strokeStyle = document.getElementById("colorblock").value;
}
//brush and it's size--------------
var size = 1;
function brush(){
    document.getElementById("m").style.cursor = "crosshair";
    pen = true;
    word = false;
    cir = false;
    tri = false;
    rect = false;
    ima = false;
    c.lineWidth = size;
    c.strokeStyle = document.getElementById("colorblock").value;
}
function addsize(){
    if(size < 20){
        c.lineWidth += 1;
        size += 1;
        document.getElementById("brush_size").value = size;
    }
}
function subsize(){
    if(size > 1){
        c.lineWidth -= 1;
        size -= 1;
        document.getElementById("brush_size").value = size;
    } 
}
function eraser(){
    pen = true;
    word = false;
    cir = false;
    tri = false;
    rect = false;
    ima = false;
    brush_line_true();
    c.strokeStyle = "#ffffff";
    c.lineWidth = size;
}
//Clear
function draw_White(){
    c.clearRect(0,0,600,400);
    c.rect(0,0,600,400);
    c.fillStyle = "white";
    c.fill();
    cPush();   
}
//Undo Redo
var cPushArray = new Array();
var cStep = -1;
function cPush(){
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(getc.toDataURL());
    //document.title = cStep + ":" + cPushArray.length;
}
function cUndo(){
    if (cStep > 0){
        cStep--;
        var canvasPic = new Image();
        c.clearRect(0,0,600,400);
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { c.drawImage(canvasPic, 0, 0); }
        //document.title = cStep + ":" + cPushArray.length;
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1){
        cStep++;
        c.clearRect(0,0,600,400);
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { c.drawImage(canvasPic, 0, 0); }
        //document.title = cStep + ":" + cPushArray.length;
    }
}
//Text
var choose_font = 1;
function text_true(){
    document.getElementById("m").style.cursor = "text";
    var size;
    size = document.getElementById("text_size").value;
    pen = false;
    word = true;
    cir = false;
    tri = false;
    rect = false;
    ima = false;
    c.font = "normal 70px Arial";
    if(choose_font == 1)
        c.font = "normal " + size + "px " + "Arial";
    else if(choose_font == 2)
        c.font = "italic " + size + "px " + "Arial";
    else
        c.font = "oblique " + size + "px " + "Arial";
}
function text(event){
    var str = document.getElementById("text").value;
    c.fillStyle = document.getElementById("colorblock").value;
    c.fillText(str,event.offsetX,event.offsetY);
}
function radio1(){choose_font = 1;}
function radio2(){choose_font = 2;}
function radio3(){choose_font = 3;}
//shapes
var now_picture = new Image();
var startX,startY;
function circle_true(){
    document.getElementById("m").style.cursor = "move";
    cir = true;
    pen = false;
    word = false;
    tir = false;
    rect = false;
    ima = false;
}
function triangle_true(){
    document.getElementById("m").style.cursor = "move";
    cir = false;
    pen = false;
    word = false;
    tri = true;
    rect = false;
    ima = false;
}
function rectangle_true(){
    document.getElementById("m").style.cursor = "move";
    cir = false;
    pen = false;
    word = false;
    tri = false;
    rect = true;
    ima = false;
}
//draw_shapes
function draw_circle(event){
    c.beginPath();
    c.strokeStyle = document.getElementById("colorblock").value;
    c.arc(event.offsetX,event.offsetY,size,0,2*Math.PI);
    c.fillStyle = document.getElementById("colorblock").value;
    c.fill();
    c.stroke();
}
function draw_triangle(event){
    c.beginPath();
    c.strokeStyle = document.getElementById("colorblock").value;
    c.moveTo(event.offsetX,event.offsetY - 1);
    c.lineTo(event.offsetX + 1,event.offsetY + 1);
    c.lineTo(event.offsetX - 1 ,event.offsetY + 1);
    c.lineTo(event.offsetX,event.offsetY - 1);
    c.lineTo(event.offsetX + 1,event.offsetY + 1);
    c.fill();
    c.stroke();
    c.closePath();
}
function draw_rectangle(){
    c.beginPath();
    c.strokeStyle = document.getElementById("colorblock").value;
    c.rect(event.offsetX-(size/2),event.offsetY-(size/2),size,size);
    c.fill();
    c.stroke();
    c.closePath();   
}
//brush_shape
var brush_cir = false,brush_rec = false,brush_tri = false,brush_line = true;
    function brush_cir_true(){
    brush_line = false;
    brush_cir = true;
    brush_rec = false;
    brush_tri = false;
}
function brush_tri_true(){
    brush_line = false;
    brush_cir = false;
    brush_rec = false;
    brush_tri = true;
}
function brush_rec_true(){
    brush_line = false;
    brush_cir = false;
    brush_rec = true;
    brush_tri = false;
}
function brush_line_true(){
    brush_line = true;
    brush_cir = false;
    brush_rec = false;
    brush_tri = false;
}
//Images
function image_true(){
    document.getElementById("m").style.cursor = "pointer";
    cir = false;
    pen = false;
    word = false;
    tri = false;
    rect = false;
    ima = true;
}